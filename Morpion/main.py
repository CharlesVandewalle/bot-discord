import discord.ext.commands as cmd
import Token
import Morpion    

bot = cmd.Bot(command_prefix='$')
morpion = Morpion.Jeu()

J1 = Morpion.Joueur(":x:")
J2 = Morpion.Joueur(":o:")
tourJ1 = False
inGame = False

@bot.event
async def on_ready():
    print("bot ready to be used")


@bot.command()
async def afficher(ctx):
    global tourJ1
    
    if inGame :
        if J2.ia :
            await ctx.send(J1.id.mention + " : :x:      vs      " + J2.id + " : :o:")
        else :
            await ctx.send(J1.id.mention + " : :x:      vs      " + J2.id.mention + " : :o:")

        if tourJ1 :
            await ctx.send("au tour de " + J1.id.mention)
        else :
            if J2.ia :
                await ctx.send("au tour de " + J2.id)
            else :
                await ctx.send("au tour de " + J2.id.mention)

    for ligne in morpion.grille :
        await ctx.send(' '.join(ligne))

async def msg_fin(ctx):
    global inGame

    if morpion.fin() :
        inGame = False
        if morpion.vainqueur == J1.symbole :
            await ctx.send("victoire de " + J1.id.mention)
        elif morpion.vainqueur == J2.symbole :
            if J2.id == "rdm" or J2.id == "IA" :
                await ctx.send("victoire de " + J2.id)
            else :
                await ctx.send("victoire de " + J2.id.mention)
        elif morpion.vainqueur == "none" :
            await ctx.send("égalité")

@bot.command()
async def play(ctx, arg: int):
    global tourJ1
    global inGame

    if inGame :
        if (tourJ1 and ctx.author == J1.id) or (not tourJ1 and ctx.author == J2.id) :
            coup = morpion.chiffreToCoup(arg)
            if morpion.jouer(coup):
                tourJ1 = not tourJ1
                await msg_fin(ctx)
                await afficher(ctx)
            else :
                return await ctx.send("coup invalide, veuillez jouer un coup valide")

            if J2.id == "rdm" and inGame :
                morpion.jouer(J2.rdm(morpion))
                tourJ1 = not tourJ1
                await msg_fin(ctx)
                await afficher(ctx)
            elif J2.id == "IA" and inGame :
                morpion.jouer(J2.MC(morpion))
                tourJ1 = not tourJ1
                await msg_fin(ctx)
                await afficher(ctx)

        elif ctx.author != J1.id and ctx.author != J2.id :
            await ctx.send(ctx.author.mention + "ne sait pas lire qu'il n'est pas dans la game ...")
        else :
            await ctx.send(ctx.author.mention + "c'est pas à toi de jouer")
    else :
        await ctx.send("aucune partie lancée, lancez en une avec $start")

@bot.command()
async def start(ctx, arg):
    global tourJ1
    global inGame

    morpion.reset()
     
    J1.identity(ctx.author)

    tourJ1 = True
    inGame = True

    if ctx.message.mentions :
        J2.identity(ctx.message.mentions[0])
        J2.ia = False
    elif arg == "rdm" :
        J2.identity("rdm")
        J2.ia = True
    elif arg == "IA" :
        J2.identity("IA")
        J2.ia = True
    
    await ctx.send("nouvelle partie : ")
    await afficher(ctx)

@bot.command()
async def ragequit(ctx):
    global inGame
    
    if inGame :
        if ctx.author == J1.id :
            await ctx.send(J1.id.mention + "a ragequit, victoire de " + J2.id.mention)
        elif ctx.author == J2.id :
            await ctx.send(J2.id.mention + "a ragequit, victoire de " + J1.id.mention)
        inGame = False
    else:
        await ctx.send(ctx.author.mention + "ragequit avant meme d'avoir commencé ... quelle baltringue !")

@bot.command()
async def tuto(ctx):
    await ctx.send("la grille se compose de 9 case tel que : ")
    await ctx.send("1 2 3")
    await ctx.send("4 5 6")
    await ctx.send("7 8 9")
    await ctx.send("pour jouer, utiliser $play <numero de la case ou vous voulez jouer>")
    await ctx.send("vous pouvez quitter à tout moment avec $ragequit")

@bot.command()
async def aide(ctx):
    await ctx.send("$aide -> affiche cette aide")
    await ctx.send("$tuto -> explique comment jouer")
    await ctx.send("$start @someone -> lance une partie vous contre someone")
    await ctx.send("$play <nombre> -> joue un coup")
    await ctx.send("$ragequit -> quitte la partie")
    await ctx.send("$afficher -> affiche la grille de jeu")

bot.run(Token.TOKEN)