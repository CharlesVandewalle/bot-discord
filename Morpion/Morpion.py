import math
import random as rdm
import copy as cp

class Jeu:
       
    def __init__(self):
        self.reset()

    def reset(self):
        c = ":white_large_square:"
        self.grille = [[c,c,c],[c,c,c],[c,c,c]]
        self.cptCoup = 0
        self.joueur = ":x:"
        self.vainqueur = "none"

    def jouer(self, coup):
        if coup[0] < 0 or coup[0] > 2 or coup[1] < 0 or coup[1] > 2 :
            return False

        if self.grille[coup[0]][coup[1]] != ":white_large_square:" :     # coup valide ?
            return False 

        self.grille[coup[0]][coup[1]] = self.joueur       # jouer le coup

        if self.joueur == ":x:" :
            self.joueur = ":o:"                             # joueur suivant
        else :
            self.joueur = ":x:" 

        self.cptCoup += 1
        return True

    def fin(self):
        if self.cptCoup >= 5 :
            if self.victoire(":x:") :
                self.vainqueur = ":x:"
                return True 
            elif self.victoire(":o:") :
                self.vainqueur = ":o:"
                return True 
        elif self.cptCoup == 9 :
            return True   

        return False 

    def victoire(self, symbole):
            # victoire par ligne
        ligne = 0
        for i in range(len(self.grille)):
            ligne = 0
            for j in range(len(self.grille[0])):
                if self.grille[i][j] == symbole:
                    ligne += 1
                if ligne == 3:
                    self.vainqueur = symbole
                    return True

            # victoire par colonne
        colonne = 0
        for i in range(len(self.grille[0])):
            colonne = 0
            for j in range(len(self.grille)):
                if self.grille[j][i] == symbole:
                    colonne += 1
                if colonne == 3:
                    self.vainqueur = symbole
                    return True

            # diagonale
        if self.grille[1][1] == symbole and (self.grille[0][0] == symbole and self.grille[2][2] == symbole or self.grille[2][0] == symbole and self.grille[0][2] == symbole):
            self.vainqueur = symbole
            return True

            # grille complete
        for l in range(len(self.grille)):
            for c in range(len(self.grille[0])):
                if self.grille[l][c] == ":white_large_square:" :
                    return False
        self.vainqueur = "none"
        return True

    def listeCoup(self):
        liste = []
        for l in range(len(self.grille)):
            for c in range(len(self.grille[0])):
                if self.grille[l][c] == ":white_large_square:" :
                    liste.append((l,c))
    
        return liste

    def chiffreToCoup(self, chiffre):
        chiffre -= 1
        ligne = math.floor(chiffre / 3)
        colonne = chiffre % 3
        return (ligne, colonne)

class Joueur:
    def __init__(self, symbole) :
        self.symbole = symbole

    def identity(self, id):
        self.id = id

    def rdm(self, jeu):
        return rdm.choice(jeu.listeCoup())

    def MC(self, jeu):
        bestWinrate = -99999
        global bestCoup
        
        for coup in jeu.listeCoup() :
            winrate = 0
            jeu1 = cp.deepcopy(jeu)
            jeu1.jouer(coup)
            
            for _ in range(1000):
                jeu2 = cp.deepcopy(jeu1)
                
                while not jeu2.fin():
                    jeu2.jouer(self.rdm(jeu2))

                if jeu2.vainqueur == self.symbole :
                    winrate += 1
                elif jeu2.vainqueur == "none" :
                    pass
                else :
                    winrate -= 1
            
            print(coup, " : ", winrate)
            if winrate > bestWinrate:
                bestWinrate = winrate
                bestCoup = coup
        print(" -- ")
        return bestCoup